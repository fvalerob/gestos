package com.example.fvalerob.gestos;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Valero on 18/11/2017.
 */

public class Ejer2Activity extends AppCompatActivity {
    private static TextView textView;
    private Gestos gestos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestos);

        textView = (TextView) findViewById(R.id.velocidad);
        gestos = (Gestos)findViewById(R.id.gestos);

    }
    public static void pintarTextView(float vX, float vY){
        textView.setText("Velocidad X: " + vX + "/ Velocidad Y:" + vY);
    }
}