package com.example.fvalerob.gestos;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Valero on 18/11/2017.
 */

public class Gestos extends View implements GestureDetector.OnGestureListener {
    private final static int WITDH= 50;
    private final static int HEIGTH = 50;

    private Paint paint;
    private float posX, posY;
    private GestureDetectorCompat mDetectorGestos;
    private float vX, vY;
    private Paint paintFling;
    private boolean colorRojo=true;

    private void inicializar(){
        mDetectorGestos = new GestureDetectorCompat(getContext(),this);
        posX = 100;
        posY = 100;
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
         //Inicializamos el color del rectangulo siempre a rojo
        paint.setColor(Color.RED);
        //Pintamos linea para el fling.
        paintFling = new Paint();
        paintFling.setStyle(Paint.Style.STROKE);
        paintFling.setStrokeWidth(3);
        paintFling.setColor(Color.RED);

    }
    //Dibujamos el rectangulo
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.WHITE);
        canvas.drawRect(posX - WITDH, posY - HEIGTH,
                posX + WITDH, posY + HEIGTH, paint);
        //Dibujamos la linea de fling
        canvas.drawLine(posX, posY,
                posX + vX, posY + vY, paintFling);
    }

    private boolean estaDentroRectangulo(float x, float y){
        if(x <= posX+WITDH && x >= posX-WITDH && y <= posY+HEIGTH && y >= posY-HEIGTH ) {
            return true;
        }else{
            return  false;
        }
    }
    public Gestos(Context context) {
        super(context);
        inicializar();
    }

    public Gestos(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inicializar();
    }

    public Gestos(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inicializar();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mDetectorGestos.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return estaDentroRectangulo(motionEvent.getX(), motionEvent.getY());
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        if(estaDentroRectangulo(motionEvent.getX(),motionEvent.getY())){
            colorRojo = !colorRojo; //Vamos poniendo el color rojo a true y false segun vayamos tocando el rectangulo.
        }
        if(colorRojo == true){
            paint.setColor(Color.RED);
        }else{
            paint.setColor(Color.BLUE);
        }
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        posX = motionEvent1.getX();
        posY = motionEvent1.getY();
        vX = 0;
        vY = 0;
        this.invalidate();
        return true;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        vX = v;
        vY = v1;
        Ejer2Activity.pintarTextView(vX,vY);

        this.invalidate();
        return true;
    }
}
