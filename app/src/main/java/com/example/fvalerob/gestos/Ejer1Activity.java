package com.example.fvalerob.gestos;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Valero on 18/11/2017.
 */

public class Ejer1Activity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new RectanguloComponent(this));
    }
}