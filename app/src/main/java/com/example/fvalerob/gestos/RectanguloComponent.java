package com.example.fvalerob.gestos;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Valero on 18/11/2017.
 */

public class RectanguloComponent extends View {

    private final static int WITDH= 50;
    private final static int HEIGTH = 50;

    private Paint paint;
    private float posX, posY;

    private void inicializar() {
        posX = 100;
        posY = 100;
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if (event.getAction() == MotionEvent.ACTION_MOVE)
        {
            //el dedo se ha movido sobre la pantalla, se tarta d eun evento de continuacion de gesto.
            posX = event.getX();
            posY = event.getY();
            this.invalidate();
        }else {
            //El dedo se acaba de poner sobre la pantalla, indica el inciio de un nuevo gesto.
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                return estaDentroRectangulo(event.getX(), event.getY());
            }
        }
        return true;
    }
    //Dibujamos el rectangulo
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.WHITE);
        canvas.drawRect(posX - WITDH, posY - HEIGTH,
                posX + WITDH, posY + HEIGTH, paint);
    }

    private boolean estaDentroRectangulo(float x, float y){
        if(x <= posX+WITDH && x >= posX-WITDH && y <= posY+HEIGTH && y >= posY-HEIGTH ) {
            return true;
        }else{
            return  false;
        }
    }

    public RectanguloComponent(Context context) {
        super(context);
        inicializar();
    }

    public RectanguloComponent(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inicializar();
    }

    public RectanguloComponent(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inicializar();
    }

}
